<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Raspi</title>
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
	<style>

		body {
			font-family: "Trebuchet MS", "Helvetica", "Arial",  "Verdana", "sans-serif";
			font-size: 62.5%;
			 
		background: #f9f9f9;
		}
		
		button
		{
			width:10em;
			margin:2px 2px 2px 2px;
		}
		.page { height:90%;}

		.ui-state-active {	background:url("images/ui-bg_glass_65_ffffff_1x400.png") repeat-x scroll 50% 50% #c6c6c6;}
		.ui-accordion-content{ background: #f9f9f9;}
	</style>
	<script src="js/jquery-1.9.1.js"></script>
	<script src="js/jquery-ui.js"></script>
</head>
<body >
<script>
	function menuElementClicked( element, cmd )
	{
		// Animation de l'élément pour notifier le click
		//element.fadeIn(100).fadeOut(100).fadeIn(100);
		$(".result").hide(); 
		// Execution de la commande
		element.find(".result").show( "drop");
		$.ajax( { url:'commande.php?commande='+cmd, context : document.body } )
			.done ( function(data) 
						{ 
							element.find(".result").text( data); 
						} 
					)
			.fail ( function(data) 
						{ 
							element.find(".result").text( data); 
						} 
					);

	}
	
	function menuClicked( element, submenu )
	{
		$(".submenu").hide();
		element.fadeIn(100).fadeOut(100).fadeIn(100);
		//element.animate({opacity:0},200,"linear",function(){ $(this).animate({opacity:1},200); });
		$(submenu).show();
	}

</script>

<div id="page" class="page">
	<div id="accordion">
		<h3>Audio</h3>
		<div>
			<div id="toolbar" class="ui-widget-header ui-corner-all">
				<button onclick="menuElementClicked( $(this).parent().parent(), 'power' )">Power</button>
				<button onclick="menuElementClicked( $(this).parent().parent(), 'pause' )">Pause</button>
				<button onclick="menuElementClicked( $(this).parent().parent(), 'previous' )">Previous</button>		
				<button onclick="menuElementClicked( $(this).parent().parent(), 'next' )">Next</button>
			</div>
			<div onclick="menuElementClicked( $(this).parent(), 'histoire' )"><button>Franck Ferrand</button></div>
			<div onclick="menuElementClicked( $(this).parent(), 'randomPlay' )"><button>Random Play</button></div>
		<p class="result">Lorem Ipsum</p>
		</div>
		<h3>Interrupteurs</h3>
		<div>
			<div onclick="menuElementClicked( $(this), 'int1' )"><button>Interrupteur 1</button><p class="result"></p></div>
			<div onclick="menuElementClicked( $(this), 'int2' )"><button>Interrupteur 2</button><p class="result"></p></div>			
		</div>
		<h3>Systeme</h3>
		<div>
			<div onclick="menuElementClicked( $(this), 'ls' )"><button>ls</button><p class="result"></p></div>					
		</div>
	</div>
</div>

<script>
jQuery( document ).ready( function() 
		{ 
			$( "button" ).button().click(function( event ) {
								event.preventDefault();
								});

			$(".result").hide(); 
			
			$( "#accordion" ).accordion({
				heightStyle: "content",
				collapsible: true,
				active: false,
				
			});

			/*
			var icons = {
				header: "ui-icon-circle-arrow-e",
				activeHeader: "ui-icon-circle-arrow-s"
			};
			*/
		} 
	);


</script>


</body></html>
