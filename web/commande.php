<?php
	$commande  = $_GET["commande"];

	$cmd      = "";
	$soap_arg = "";
	switch($commande)
	{
		case "next"       :  $cmd = "/usr/local/ir2squeeze/next"; break;
		case "histoire"   :  $cmd = "/usr/local/ir2squeeze/histoire"; break;
		case "pause"      :  $cmd = "/usr/local/ir2squeeze/pause"; break;
		case "power"      :  $cmd = "/usr/local/ir2squeeze/power"; break;
		case "previous"   :  $cmd = "/usr/local/ir2squeeze/previous"; break;
		case "randomPlay" :  $cmd = "/usr/local/ir2squeeze/randomPlay"; break;
		case "int1on"     :  $soap_arg = array( "marque" => "AVIDSEN", "numero" => 1, "commande" => "ON"); break;
		case "int1off"    :  $soap_arg = array( "marque" => "AVIDSEN", "numero" => 1, "commande" => "OFF"); break;
		case "int2on"     :  $soap_arg = array( "marque" => "AVIDSEN", "numero" => 2, "commande" => "ON"); break;
		case "int2off"    :  $soap_arg = array( "marque" => "AVIDSEN", "numero" => 2, "commande" => "OFF"); break;
		case "int3on"     :  $soap_arg = array( "marque" => "AVIDSEN", "numero" => 3, "commande" => "ON"); break;
		case "int3off"    :  $soap_arg = array( "marque" => "AVIDSEN", "numero" => 3, "commande" => "OFF"); break;
		case "int4on"     :  $soap_arg = array( "marque" => "AVIDSEN", "numero" => 4, "commande" => "ON"); break;
		case "int4off"    :  $soap_arg = array( "marque" => "AVIDSEN", "numero" => 4, "commande" => "OFF"); break;
		case "blyss1on"   :  $soap_arg = array( "marque" => "BLYSS", "numero" => 1, "commande" => "ON"); break;
		case "blyss1off"  :  $soap_arg = array( "marque" => "BLYSS", "numero" => 1, "commande" => "OFF"); break;
		case "blyss2on"   :  $soap_arg = array( "marque" => "BLYSS", "numero" => 2, "commande" => "ON"); break;
		case "blyss2off"  :  $soap_arg = array( "marque" => "BLYSS", "numero" => 2, "commande" => "OFF"); break;
		case "blyss3on"   :  $soap_arg = array( "marque" => "BLYSS", "numero" => 3, "commande" => "ON"); break;
		case "blyss3off"  :  $soap_arg = array( "marque" => "BLYSS", "numero" => 3, "commande" => "OFF"); break;
		case "blyss4on"   :  $soap_arg = array( "marque" => "BLYSS", "numero" => 4, "commande" => "ON"); break;
		case "blyss4off"  :  $soap_arg = array( "marque" => "BLYSS", "numero" => 4, "commande" => "OFF"); break;
		case "blyss5on"   :  $soap_arg = array( "marque" => "BLYSS", "numero" => 5, "commande" => "ON"); break;
		case "blyss5off"  :  $soap_arg = array( "marque" => "BLYSS", "numero" => 5, "commande" => "OFF"); break;
		case "blyss6on"   :  $soap_arg = array( "marque" => "BLYSS", "numero" => 6, "commande" => "ON"); break;
		case "blyss6off"  :  $soap_arg = array( "marque" => "BLYSS", "numero" => 6, "commande" => "OFF"); break;
		
		case "ls"         :  $cmd = "ls -lap"; break;
		default:
			$cmd = "";
			break;	
	}
	if ( $cmd != "" )
	{
		$output = array();
		$rv = 0;
		exec( $cmd, $output, $rv );
		print_r( $output );
	}
	else if ( $soap_arg != "" )
	{
		$url = "http://localhost:8080/sendRF";
		$fonction = "sendRF";

		$client = new SoapClient(null
								, array('location' => $url
									, 'uri'      => "RF"
									, 'trace' => 1 ) 
					);
		$return =  $client->__soapCall ( $fonction, $soap_arg, array('soapaction' => $fonction) );
		echo $client->__getLastResponse() ;
	}
	else
	{
		echo "commande [$commande] inconnue";
	}
?>
