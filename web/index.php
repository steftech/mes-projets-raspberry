<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Raspi</title>
	<style>
		.rootmenu { width: 10em; position: absolute; border:1px; }
		.submenuContainer {}
		.submenu { left: 11em; width: 10em; position: absolute; }
		.resultBar { text-align: center; padding-bottom: 1ex; }
		.menuContainer	{ width:100%; height:80%}
	
		.menuElementColor {
			border: 1px solid #aaaaaa;
			background-image:linear-gradient(60deg, #A97BED, white);
			color: #222222;
			font-family: Arial;
			box-shadow: 0 0 5px rgba( 0, 0, 0, 0.5), 0 -1px 0 rgba( 255, 255, 255, 0.4);
		}

		.menuElementSize
		{
			margin:5px 0px 0px 0px;
			padding:0px 0px 0px 5px;
			border-top-left-radius: 4px;
			border-top-right-radius: 4px;
			border-bottom-left-radius: 4px;
			border-bottom-right-radius: 4px;
		}
		
		.menuElement{ 
			font-family: Arial,sans-serif; 
			font-size: 1em; 
			width: 10em;
			height: 1em; 
			padding-top: 0px; /*permet le centrage vertical*/ 
			text-align: center; 
			color: #ffffff; 
			background: #444; 
			background: -webkit-linear-gradient( #555, #2C2C2C); 
			background: -moz-linear-gradient( #555, #2C2C2C); 
			background: -ms-linear-gradient( #555, #2C2C2C); 
			background: -o-linear-gradient( #555, #2C2C2C); 
			background: linear-gradient( #555, #2C2C2C);
			border-radius: 8px;
			text-shadow: 0px 1px 0px rgba( 10, 10, 10, 0.2);
			box-shadow: 0 0 5px rgba( 0, 0, 0, 0.5), 0 -1px 0 rgba( 255, 255, 255, 0.4);
		}
		
		
	</style>
	<script src="js/jquery-1.9.1.js"></script>
	<script src="js/jquery-ui.js"></script>
</head>
<body >
<script>
	function menuElementClicked( element, cmd )
	{
		// Animation de l'élément pour notifier le click
		element.fadeIn(100).fadeOut(100).fadeIn(100);
		// Execution de la commande
		$.ajax( { url:'commande.php?commande='+cmd, context : document.body } )
			.done ( function(data) { $("#result").text( data); } )
			.fail ( function(data) { $("#result").text( data); } );
	}
	
	function menuClicked( element, submenu )
	{
		$(".submenu").hide();
		element.fadeIn(100).fadeOut(100).fadeIn(100);
		//element.animate({opacity:0},200,"linear",function(){ $(this).animate({opacity:1},200); });
		$(submenu).show();
	}

</script>

<div id="page">
	<div id="menu" class="menuContainer">
		<div class="rootmenu">
			<div id="menu1"><p class="menuElementColor roundCornerAll menuElementSize" onclick='menuClicked( $(this), "#submenu1" )'>Audio</p></div>
			<div id="menu2"><p class="menuElementColor roundCornerAll menuElementSize" onclick='menuClicked( $(this), "#submenu2" )'>Interrupteurs</p></div>
			<div id="menu3"><p class="menuElementColor roundCornerAll menuElementSize" onclick='menuClicked( $(this), "#submenu3" )'>Interrupteurs Blyss</p></div>
			<div id="menu4"><p class="menuElementColor roundCornerAll menuElementSize" onclick='menuClicked( $(this), "#submenu4" )'>Système</p></div>
		</div>
		<div class="submenuContainer">
			<div style="display: none;" class="submenu" id="submenu1">
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'next' )">Next</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'previous' )">Previous</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'pause' )">Pause</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'power' )">Power</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'histoire' )">Franck Ferrand</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'randomPlay' )">Random Play</p></div>
			</div>
			<div style="display: none;" class="submenu" id="submenu2">
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'int1on' )">Interrupteur 1 ON</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'int1off' )">Interrupteur 1 OFF</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'int2on' )">Interrupteur 2 ON</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'int2off' )">Interrupteur 2 OFF</p></div>
			</div>
			<div style="display: none;" class="submenu" id="submenu3">
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'blyss1on' )">Blyss 1 ON</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'blyss1off' )">Blyss 1 OFF</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'blyss2on' )">Lampe ambiance ON</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'blyss2off' )">Lampe ambiance OFF</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'blyss3on' )">Lampe colorée ON</p></div>
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'blyss3off' )">Lampe colorée OFF</p></div>
				
			</div>
			<div style="display: block;" class="submenu" id="submenu4">
				<div><p class="menuElementColor roundCornerAll menuElementSize" onclick="menuElementClicked( $(this), 'ls' )">ls</p></div>

			</div>
		</div>
		
	</div>
	<br>
	<div id="result" class="resultBar">
	</div>
</div>
<script>
jQuery( document ).ready( function() { $(".submenu").hide(); } );
$("#page").height ( $(window).height() * 90 / 100 );
</script>


</body></html>
