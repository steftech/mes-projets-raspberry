#!/usr/bin/python

import os
import SOAPpy
from RF import RF
from Blyss import Blyss
import time

def sendRF( param0, param1, param2 ):
	commande = "RF " + str( param0) + " " + str( param1 ) + " " + str( param2 )
	print commande
	if param0 == "BLYSS":
		rf = Blyss( 17 )
		rf.send( str(param1), str(param2) )
	else :
		rf = RF( 17 )
		rf.send( param0, str( param1 ) + " " + str( param2 ) )
	
	return commande



server = SOAPpy.SOAPServer(("localhost", 8080))
server.registerFunction(sendRF, "RF")
server.serve_forever()
