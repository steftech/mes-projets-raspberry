#!/usr/bin/python
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time
import sys



class RF :
	
	tableau = { "AVIDSEN" : 
				{ "echantillonnage" : 44100,
				  "bits" : 
					{ 0 : [ [ 52, 0 ], [ 22, 1 ] ] ,
					  1 : [ [ 22, 0 ], [ 52, 1 ] ] },
				  "buttons" : 
					{ "1 ON"  : "011111111111100101101",
					  "1 OFF" : "011111111111100111100",
					  "2 ON"  : "011111111111110101111",
					  "2 OFF" : "011111111111110111110",
					  "3 ON"  : "011111111111111101110",
					  "3 OFF" : "011111111111111111111",
					  "4 ON"  : "011111111111100001111",
					  "4 OFF" : "011111111111100011110",
					  
					}
				}
			}
	
	paramBits = {}
	dataPin    = 17
	tempsPerdu = 0
	nb_retry   = 4
	# Nombre de secondes d'attente entre deux retry
	attenteEntrePaquets = 0.075
	
	def __init__(self, dataPin ):
		self.dataPin = dataPin
		GPIO.setwarnings(False) 
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(self.dataPin, GPIO.OUT)

	def sendDataPulse( self, dataPin, duration, value ):
		if value == 1 :
			GPIO.output(dataPin, GPIO.HIGH)
		else:
			GPIO.output(dataPin, GPIO.LOW)
		time.sleep( duration )

	def checkConfig( self, tableau ):
		for marque in self.tableau :
			if not 0 in self.tableau[marque]["bits"] :
				print "erreur paramétrage bit 0  pour la marque %s"% ( marque )
				exit( 1 )
			
			if not 1 in self.tableau[marque]["bits"] :
				print "erreur paramétrage bit 1  pour la marque %s"% ( marque )
				exit( 1 )

			if not "echantillonnage" in self.tableau[marque] :
				print "echantillonnage mal paramétré pour la marque %s" % ( marque )
				exit( 1 )


	def checkParam( self, marque, button ):
		if not marque in self.tableau :
			print "marque %s inconnue" % ( marque )
			exit( 1 )

		if not button in self.tableau[marque]["buttons"] :
			print "boutton %s inconnu  pour la marque %s" % ( button, marque )
			exit( 1 )

	def send( self, marque, button ):
		
		# Précalcul les attentes pour chaque type de bits
		for valeur in range(0, 2):
			self.paramBits[ valeur ] = [] 
			for paramBit in self.tableau[marque]["bits"][valeur]:
				tempsAttente  = 1.0 * ( paramBit[0] - self.tempsPerdu) / self.tableau[marque]["echantillonnage"]
				self.paramBits[valeur].append( [ tempsAttente , paramBit[1] ] )
		
		self.sendDataPulse( self.dataPin, 0.01 , 0 )
		for i in range( 0, self.nb_retry ) :
			for bit in self.tableau[marque]["buttons"][ button ]:
				for paramBit in self.paramBits[ int( bit ) ]:
					self.sendDataPulse ( self.dataPin, paramBit[0], paramBit[1])
			self.sendDataPulse( self.dataPin, 0.01 , 0 )
			time.sleep( self.attenteEntrePaquets )
		self.sendDataPulse( self.dataPin, 0.01 , 0 )

	def __del__(self):
		GPIO.cleanup()

if __name__ == "__main__" :
	marque = "AVIDSEN"
	button = "1 ON"
	
	if len(sys.argv) > 2:
		marque = sys.argv[1]
		button = sys.argv[2] + " " + sys.argv[3]
	else :
		print "arguments incorrects :"
		print sys.argv[0] + " <marque> <id> <position>"
		sys.exit(1)
	


	rf = RF( 17 )
	rf.send( marque, button )
			
	
	

